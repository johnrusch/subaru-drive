import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { LifestyleComponent } from './sub-components/lifestyle/lifestyle.component';
import { VehicleComponent } from './sub-components/vehicle/vehicle.component';
import { OwnerguideComponent } from './sub-components/ownerguide/ownerguide.component';
import { MagazinesComponent } from './sub-components/magazines/magazines.component';
import { MainComponent } from './sub-components/main/main.component';
import { ArticlesThirdComponent } from './sub-components/card-components/articles-third/articles-third.component';
import { ArticleHeroComponent } from './sub-components/card-components/article-hero/article-hero.component';
import { ArticlesHalfComponent } from './sub-components/card-components/articles-half/articles-half.component';
import { ArticleHalfComponent } from './sub-components/card-components/articles-half/article-half/article-half.component';
import { MainCategoryComponent } from './sub-components/full-components/main-category/main-category.component';
import { SectionComponent } from './sub-components/ownerguide/section/section.component';
import { ArticleComponent } from './sub-components/ownerguide/section/article/article.component';
import { ArticlesComponent } from './sub-components/articles/articles.component';
import { TopNavComponent } from './layout/top-nav/top-nav.component';
import { FooterComponent } from './layout/footer/footer.component';
import { SocialLinksComponent } from './layout/footer/social-links/social-links.component';

const appRoutes: Routes = [
	{ path: '', component: MainComponent, },
	{ path: 'lifestyle', component: LifestyleComponent },
	{ path: 'vehicle', component: VehicleComponent },
	{ path: 'ownerguide', component: OwnerguideComponent },
	{ path: 'ownerguide/:section', component: OwnerguideComponent },
	{ path: 'ownerguide/:section/:article', component: OwnerguideComponent },
	{ path: 'magazines', component: MagazinesComponent },
	{ path: 'articles/:alias', component: ArticlesComponent }
];

@NgModule({
	declarations: [
		AppComponent,
		LifestyleComponent,
		VehicleComponent,
		MainComponent,
		ArticlesThirdComponent,
		ArticleHeroComponent,
		ArticlesHalfComponent,
		ArticleHalfComponent,
		MainCategoryComponent,
		OwnerguideComponent,
		SectionComponent,
		ArticleComponent,
  ArticlesComponent,
  TopNavComponent,
  FooterComponent,
  SocialLinksComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		RouterModule.forRoot(appRoutes)
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
