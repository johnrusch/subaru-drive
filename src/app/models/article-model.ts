import { BaseArticleModel } from "./base-article-model";
import { MediaGalleryItemModel } from "./media-gallery-item-model";

export class ArticleModel extends BaseArticleModel {
	public body: string = "";
	public mastImageCaption: string = "";
	public authorSlug: string = "";
	public byline: string = "";
	public mediaGalleryText: string = "";
	public socialMediaText: string = "";
	public publishDate: Date = new Date();
	public publicationInfo: string = "";
	public expandedOnlineContent: string = "";
	public mediaGallery: MediaGalleryItemModel[] = [];
}
