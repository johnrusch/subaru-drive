export class BaseArticleModel {
	public alias: string = "";
	public contentItemId: string = "";
	public title: string = "";
	public teaserBody: string = "";
	public isEditorPick: boolean = false;
	public isFeatureArticle: boolean = false;
	public isHeroArticle: boolean = false;
	public category: string = "";
	public subcategory: string = "";
	public mastImage: URL = new URL("");
	public magazineIssueID: string = "";
	public interests: string[] = [];
}
