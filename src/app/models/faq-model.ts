export class FaqModel {
	public question: string = "";
	public answer: string = "";
	public isActive: boolean = false;
}