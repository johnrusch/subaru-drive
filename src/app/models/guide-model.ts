export class GuideModel {
	public title: string = "";
	public body: string = "";
	public mastImage: string = "";
	public guideSections: GuideSection[] = [];
}

export class GuideSection {
	public title: string = "";
	public hilightColor: string = "";
	public guideSectionArticles: GuideSectionArticle[] = [];
}

export class GuideSectionArticle {
	public title: string = "";
	public leftPanelBody: string = "";
	public body: string = "";
	public referencedArticles: ReferencedArticle[] = [];
}

class ReferencedArticle {
	public title: string = "";
	public contentItemId: string = "";
	public alias: string = "";
	public mastImage: string = "";
}
