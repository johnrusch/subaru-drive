export class MagazineModel {
	public title: string = "";
	public publishDate: Date = new Date();
	public mastImage: URL = new URL("/");
	public coverImage: URL = new URL("/");
	public issueId: string = "";
}