export class MediaGalleryItemModel {
	public alternateText: string = "";
	public mediaCredit: string = "";
	public imageUrl: URL = new URL("/");
	public imageCaption: string = "";
}