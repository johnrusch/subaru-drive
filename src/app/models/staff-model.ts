export class StaffModel {
	public name: string = "";
	public position: string = "";
	public photoUrl: URL = new URL("/");
}