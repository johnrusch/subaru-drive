import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ArticleModel } from '../models/article-model';
import { BaseArticleModel } from '../models/base-article-model';
import { FaqModel } from '../models/faq-model';
import { GuideModel } from '../models/guide-model';
import { MagazineModel } from '../models/magazine-model';
import { StaffModel } from '../models/staff-model';
import { ApiResponse } from './models/api-response-model';
import { BaseArticleQueryModel } from './models/base-article-query-model';
import { TaxonomyResponseModel } from './models/taxonomy-response-model';

export enum TaxonomyName {
	Categories = "Categories",
	Subcategories = "Subcategories",
	Interests = "Interests"
}

@Injectable({
	providedIn: 'root'
})
export class DriveWebApiService {

	public constructor(private http: HttpClient) { }

	public async getMainFAQAsync() : Promise<FaqModel[] | null> {
		try {
			const faqs = await firstValueFrom(
				this.http.get<ApiResponse<FaqModel[]>>(environment.webApiMainFaq));
			return faqs.data ? faqs.data : null;
		} catch(error) {
			console.error("getMainFAQAsync", error);
		}
		return null;
	}

	public async getMainStaffAsync() : Promise<StaffModel[] | null> {
		try {
			const staff = await firstValueFrom(
				this.http.get<ApiResponse<StaffModel[]>>(environment.webApiMainStaff));
			return staff.data ? staff.data : null;
		} catch(error) {
			console.error("getMainStaffAsync", error);
		}
		return null;
	}

	public async getMagazinesAsync() : Promise<MagazineModel[] | null> {
		try {
			const magazines = await firstValueFrom(
				this.http.get<ApiResponse<MagazineModel[]>>(environment.webApiMagazines));
			return magazines.data ? magazines.data : null;
		} catch(error) {
			console.error("getMagazinesAsync", error);
		}
		return null;
	}

	public async getBaseArticlesAsync(
		queryParams: BaseArticleQueryModel | null = null) : Promise<BaseArticleModel[] | null> {

		const httpParams = queryParams ? new HttpParams( { fromObject: queryParams } ) : undefined;
		
		try {
			const articles = await firstValueFrom(
				this.http.get<ApiResponse<BaseArticleModel[]>>(
					environment.webApiBaseArticles, { params: httpParams}));
			return articles.data ? articles.data : null;
		} catch(error) {
			console.error("getBaseArticlesAsync", error);
		}
		return null;
	}

	public async getArticleByContentItemIdAsync(contentItemId: string): Promise<ArticleModel | null> {
		const targetUrl = new URL(`contentItemId/${contentItemId}`, environment.webApiBaseArticles);
		try {
			const article = await firstValueFrom(
				this.http.get<ApiResponse<ArticleModel>>(targetUrl.toString()));
			return article.data ? article.data : null;
		} catch(error) {
			console.error("getArticleByContentItemIdAsync", error);
		}
		return null;
	}

	public async getArticleByAliasAsync(alias: string): Promise<ArticleModel | null> {
		const targetUrl = new URL(`alias/${alias}`, environment.webApiBaseArticles);
		try {
			const article = await firstValueFrom(
				this.http.get<ApiResponse<ArticleModel>>(targetUrl.toString()));
			return article.data ? article.data : null;
		} catch(error) {
			console.error("getArticleByAliasAsync", error);
		}
		return null;
	}

	public async getTaxonomyAsync(name: TaxonomyName): Promise<string[] | null> {
		const targetUrl = new URL(name, environment.webApiTaxonomies);
		try {
			const article = await firstValueFrom(
				this.http.get<ApiResponse<TaxonomyResponseModel>>(targetUrl.toString()));
			return article.data?.taxonomyTerms ? article.data.taxonomyTerms : null;
		} catch(error) {
			console.error("getTaxonomyAsync", error);
		}
		return null;
	}

	public async getOwnerGuideAsync() : Promise<GuideModel | null> {
		try {
			const ownerGuide = await firstValueFrom(
				this.http.get<ApiResponse<GuideModel>>(environment.webApiOwnerGuide));
			return ownerGuide.data ? ownerGuide.data : null;
		} catch(error) {
			console.error("getOwnerGuideAsync", error);
		}
		return null;
	}
}
