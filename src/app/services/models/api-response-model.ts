export class ApiResponse<TData> {
	public count?: Number;
	public data?: TData;
}
