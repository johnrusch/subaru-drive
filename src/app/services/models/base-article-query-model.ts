export type BaseArticleQueryModel = {
	skip: number;
	take: number;
	category: string;
	subcategory: string;
	interests: string[];
	magazineIssueId: string;
	hero: boolean;
	featured: boolean;
	editorPick: boolean;
	randomize: boolean;
}