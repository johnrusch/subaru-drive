export class TaxonomyResponseModel{
	public name: string = "";
	public taxonomyTerms: string[] = [];
}