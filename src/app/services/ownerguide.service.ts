import { Injectable } from '@angular/core';
import { GuideModel, GuideSection, GuideSectionArticle } from '../models/guide-model';
import { DriveWebApiService } from './drive-web-api.service';

@Injectable({
	providedIn: 'root'
})
export class OwnerguideService {

	private readonly _guideModel: Promise<GuideModel | null>;

	constructor(webApi: DriveWebApiService) {
		this._guideModel = webApi.getOwnerGuideAsync();
	}

	public getGuideDataAsync() : Promise<GuideModel | null>{
		return this._guideModel;
	}

	public async getSectionAsync(sectionTitle: string)
		: Promise<GuideSection | undefined> {
		const og = await this.getGuideDataAsync();
		return og?.guideSections.find(x => x.title==sectionTitle);
	}

	public async getSectionArticleAsync(sectionTitle: string, articleTitle: string)
		: Promise<GuideSectionArticle | undefined> {
		const section = await this.getSectionAsync(sectionTitle);
		return section?.guideSectionArticles.find(x => x.title == articleTitle);
	}

	public async getDefaultSectionTitleAsync() : Promise<string | undefined>{
		const og = await this.getGuideDataAsync();
		return og?.guideSections[0]?.title;
	}

	public async getDefaultSectionArticleTitleAsync() : Promise<string | undefined>{
		const og = await this.getGuideDataAsync();
		return og?.guideSections[0]?.guideSectionArticles[0]?.title;
	}

	public async getDefaultSectionArticleTitleForSectionAsync(sectionTitle: string)
		: Promise<string | undefined> {
		const gg = await this.getSectionAsync(sectionTitle);
		return gg?.guideSectionArticles[0]?.title;
	}
}
