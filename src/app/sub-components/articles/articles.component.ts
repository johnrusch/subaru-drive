import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { DriveWebApiService } from 'src/app/services/drive-web-api.service';

type ArticleParams = {
	alias: string;
}

@Component({
	selector: 'app-articles',
	templateUrl: './articles.component.html',
	styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit, OnDestroy {

	private _currentAlias : string | null = null;
	private _subscriptionParams: Subscription | undefined;
	
	constructor(
		private activatedRoute: ActivatedRoute,
		private driveWebApi: DriveWebApiService
		) {
		this._subscriptionParams = activatedRoute.params.subscribe(async params =>
			await this.onParamsUpdatedAsync(params as ArticleParams));
	}

	async ngOnInit(): Promise<void> {
		await this.onParamsUpdatedAsync(this.activatedRoute.snapshot.params as ArticleParams);
	}

	private async onParamsUpdatedAsync(params: ArticleParams) : Promise<void> {
		if(params?.alias && (this._currentAlias !== params.alias)){
			console.log("alias", params.alias);

			this._currentAlias = params.alias;
			const test = await this.driveWebApi.getArticleByAliasAsync(params.alias);
			console.log(test);
		}
	}

	ngOnDestroy(): void {
		this._subscriptionParams?.unsubscribe();
	}
}
