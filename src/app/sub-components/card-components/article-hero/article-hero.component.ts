import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleModel } from 'src/app/models/article-model';
import { BaseArticleModel } from 'src/app/models/base-article-model';
import { DriveWebApiService } from 'src/app/services/drive-web-api.service';

@Component({
	selector: 'app-article-hero',
	templateUrl: './article-hero.component.html',
	styleUrls: ['./article-hero.component.scss'],
})
export class ArticleHeroComponent {

	private _article: BaseArticleModel | undefined;

	@Input() public set article(value: BaseArticleModel | undefined) {
		this._article = value;
		(async () => await this.setHeroArticleAsync())();
	};

	public heroArticle: ArticleModel | null = null;

	constructor(private webApi: DriveWebApiService,
		private router: Router) { }

	private async setHeroArticleAsync() : Promise<void> {
		if(this._article?.contentItemId) {
			this.heroArticle = await this.webApi.getArticleByContentItemIdAsync(this._article.contentItemId);
		}
	}

	public onReadMore() : void {
		this.router.navigate(["articles", this._article?.alias]);
	}
}
