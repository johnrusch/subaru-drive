import { Component, Input, OnInit } from '@angular/core';
import { BaseArticleModel } from 'src/app/models/base-article-model';

@Component({
	selector: 'app-article-half',
	templateUrl: './article-half.component.html',
	styleUrls: ['./article-half.component.scss'],
	host: { "class": "col-md-6 mb-3 bg-light" }
})
export class ArticleHalfComponent implements OnInit {

	@Input() article: BaseArticleModel | undefined;

	constructor() { }

	ngOnInit(): void {
	}

}
