import { Component, Input, ViewChild, ViewContainerRef } from '@angular/core';
import { BaseArticleModel } from 'src/app/models/base-article-model';
import { ArticleHalfComponent } from './article-half/article-half.component';

@Component({
	selector: 'app-articles-half',
	templateUrl: './articles-half.component.html',
	styleUrls: ['./articles-half.component.scss']
})
export class ArticlesHalfComponent {

	private static readonly ShowArticleCount: number = 6;

	private _onIndex: number = 0;
	private _articles: BaseArticleModel[] | undefined;

	@Input() public set articles(value: BaseArticleModel[] | undefined) {
		this._articles = value;
		if (this._onIndex == 0) {
			this.showMoreArticles(ArticlesHalfComponent.ShowArticleCount);
		}
	}
	public get articles(): BaseArticleModel[] | undefined {
		return this._articles;
	}

	@ViewChild('articlesHost', { read: ViewContainerRef }) private _articlesHost: ViewContainerRef | undefined;

	constructor() { }

	public onShowMoreClick(): void {
		this.showMoreArticles(ArticlesHalfComponent.ShowArticleCount);
	}

	private showMoreArticles(count: number): void {
		if (this.articles && this._articlesHost) {
			const oi = this._onIndex;
			for (let i = oi; i < Math.min(this.articles?.length, oi + count); ++i, ++this._onIndex) {
				const articleHalf = this._articlesHost.createComponent(ArticleHalfComponent);
				articleHalf.instance.article = this.articles[i];
			}
		}
	}
}
