import { Component, Input, OnInit } from '@angular/core';
import { BaseArticleModel } from 'src/app/models/base-article-model';

@Component({
	selector: 'app-articles-third',
	templateUrl: './articles-third.component.html',
	styleUrls: ['./articles-third.component.scss']
})
export class ArticlesThirdComponent implements OnInit {

	@Input() articles: BaseArticleModel[] | undefined;

	constructor() { }

	ngOnInit(): void {
		//
	}

}
