import { Component, Input, OnInit } from '@angular/core';
import { BaseArticleModel } from 'src/app/models/base-article-model';
import { DriveWebApiService } from 'src/app/services/drive-web-api.service';
import { BaseArticleQueryModel } from 'src/app/services/models/base-article-query-model';

@Component({
	selector: 'app-main-category',
	templateUrl: './main-category.component.html',
	styleUrls: ['./main-category.component.scss']
})
export class MainCategoryComponent implements OnInit {

	@Input() categoryName: string | undefined;

	featuredArticles: BaseArticleModel[] | undefined;
	heroArticle: BaseArticleModel | undefined;
	allArticles: BaseArticleModel[] | undefined;

	constructor(private webApi: DriveWebApiService) { }

	public async ngOnInit(): Promise<void> {
		
		const allArticlesForCategory = await this.webApi.getBaseArticlesAsync({
			category: this.categoryName,
			randomize: true
		} as BaseArticleQueryModel);

		this.featuredArticles = allArticlesForCategory?.filter(
			x => x.isFeatureArticle && !x.isHeroArticle).slice(0,3);

		const arr = allArticlesForCategory?.filter(x => x.isHeroArticle);

		if(arr && (arr.length > 0)){
			this.heroArticle = arr[0];
		}

		this.allArticles = allArticlesForCategory?.filter(x =>
			!x.isEditorPick && !x.isFeatureArticle && !x.isHeroArticle);
	}

}
