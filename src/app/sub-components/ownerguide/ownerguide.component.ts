import { Component, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { GuideModel, GuideSectionArticle } from 'src/app/models/guide-model';
import { OwnerguideService } from 'src/app/services/ownerguide.service';
import { SectionComponent } from './section/section.component';

type NavParams = {
	section: string;
	article: string;
}

@Component({
	selector: 'app-ownerguide',
	templateUrl: './ownerguide.component.html',
	styleUrls: ['./ownerguide.component.scss']
})
export class OwnerguideComponent implements OnDestroy, OnInit {

	public readonly OwnerGuidePathName: string = "/ownerguide";
	public ownerGuide: GuideModel | null = null;

	private _subscriptionParams: Subscription | undefined;
	@ViewChild("articlesHost", { read: ViewContainerRef }) private _articlesHost: ViewContainerRef | undefined;
	private _childSectionComponent: SectionComponent | undefined;

	constructor(
		private ownerGuideSerivce: OwnerguideService,
		private activatedRoute: ActivatedRoute,
		private router: Router) {
		this._subscriptionParams = activatedRoute.params.subscribe(async params =>
			await this.onParamsUpdatedAsync(params as NavParams));
	}

	public async ngOnInit(): Promise<void> {
		this.ownerGuide = await this.ownerGuideSerivce.getGuideDataAsync();
		const params = this.activatedRoute.snapshot.params as NavParams;

		if (params.section) {
			if (!await this.checkSectionArticleAsync(params)) {
				return;
			}
			await this.loadAndInitializeChildSectionAsync(params);
		}
	}

	private async onParamsUpdatedAsync(params: NavParams): Promise<void> {
		if (params.section) {
			if (!await this.checkSectionArticleAsync(params)) {
				return;
			}
			await this.loadAndInitializeChildSectionAsync(params);
		} else {
			// Navigate to default section and article
			var defaultSectionTitle = await this.ownerGuideSerivce.getDefaultSectionTitleAsync();
			var defaultSectionArticleTitle = await this.ownerGuideSerivce.getDefaultSectionArticleTitleAsync();
			if (defaultSectionTitle && defaultSectionTitle) {
				this.router.navigate(
					[this.OwnerGuidePathName, defaultSectionTitle, defaultSectionArticleTitle]);
			}
		}
	}

	private async loadAndInitializeChildSectionAsync(navParams: NavParams): Promise<void> {
		if (!navParams.section) { return; }

		const section = await this.ownerGuideSerivce.getSectionAsync(navParams.section);

		let article: GuideSectionArticle | undefined;
		if (section && navParams.article) {
			article = await this.ownerGuideSerivce.getSectionArticleAsync(navParams.section, navParams.article);
		}

		if (this._articlesHost) {
			if (!this._childSectionComponent) {
				this._childSectionComponent = this._articlesHost.createComponent(SectionComponent).instance;
			}
			this._childSectionComponent.ownerGuidePathName = this.OwnerGuidePathName;
			this._childSectionComponent.guideSection = section;
			this._childSectionComponent.guideSectionArticle = article;
		}
	}

	private async checkSectionArticleAsync(params: NavParams): Promise<boolean> {
		if (params.section && !params.article) {
			const defaultSectionArticleTitle = await this.ownerGuideSerivce
				.getDefaultSectionArticleTitleForSectionAsync(params.section);
			if (defaultSectionArticleTitle) {
				this.router.navigate([this.OwnerGuidePathName, params.section, defaultSectionArticleTitle]);
				return false;
			}
		}
		return true;
	}

	public ngOnDestroy(): void {
		this._subscriptionParams?.unsubscribe();
	}
}
