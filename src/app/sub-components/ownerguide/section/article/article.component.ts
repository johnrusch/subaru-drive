import { Component } from '@angular/core';
import { DomSanitizer, SafeValue } from '@angular/platform-browser';
import { GuideSectionArticle } from 'src/app/models/guide-model';

@Component({
	selector: 'app-article',
	templateUrl: './article.component.html',
	styleUrls: ['./article.component.scss']
})
export class ArticleComponent {

	private _guideSectionArticle: GuideSectionArticle | undefined;

	guideSectionBody: SafeValue | undefined;
	guideSectionLeftPanelBody: SafeValue | undefined;

	public set guideSectionArticle(value: GuideSectionArticle | undefined) {

		this._guideSectionArticle = value;
		if (this._guideSectionArticle?.body) {
			this.guideSectionBody = this.sanitizer.bypassSecurityTrustHtml(
				this._guideSectionArticle.body);
		}else{
			this.guideSectionBody = undefined;
		}
		if (this._guideSectionArticle?.leftPanelBody) {
			this.guideSectionLeftPanelBody = this.sanitizer.bypassSecurityTrustHtml(
				this._guideSectionArticle.leftPanelBody);
		} else {
			this.guideSectionLeftPanelBody = undefined;
		}
	}

	public get guideSectionArticle(): GuideSectionArticle | undefined {
		return this._guideSectionArticle;
	}

	constructor(private sanitizer: DomSanitizer) {

	}
}
