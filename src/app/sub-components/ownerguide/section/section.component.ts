import { AfterContentChecked, Component, ViewChild, ViewContainerRef } from '@angular/core';
import { GuideSection, GuideSectionArticle } from 'src/app/models/guide-model';
import { ArticleComponent } from './article/article.component';

@Component({
	selector: 'app-section',
	templateUrl: './section.component.html',
	styleUrls: ['./section.component.scss']
})
export class SectionComponent implements AfterContentChecked {

	public ownerGuidePathName: string | undefined;
	public guideSection: GuideSection | undefined;
	public guideSectionArticle: GuideSectionArticle | undefined;

	@ViewChild("articleHost", { read: ViewContainerRef }) private _articleHost: ViewContainerRef | undefined;
	private _childArticleComponent: ArticleComponent | undefined;

	ngAfterContentChecked(): void {

		if (this._articleHost && !this._childArticleComponent) {
			this._childArticleComponent = this._articleHost.createComponent(ArticleComponent).instance;
		}

		if (this._childArticleComponent &&
			(this._childArticleComponent.guideSectionArticle?.title !== this.guideSectionArticle?.title)) {
			this._childArticleComponent.guideSectionArticle = this.guideSectionArticle;
		}
	}
}
