// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	webApiMainFaq: "https://localhost:44393/api/MainFAQ/",
	webApiMainStaff: "https://localhost:44393/api/MainStaff/",
	webApiMagazines: "https://localhost:44393/api/MainMagazines/",
	webApiBaseArticles: "https://localhost:44393/api/MainArticles/",
	webApiTaxonomies: "https://localhost:44393/api/Taxonomies/",
	webApiOwnerGuide: "https://localhost:44393/api/OwnerGuide/"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
